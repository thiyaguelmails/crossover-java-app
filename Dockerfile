# Dockerfile

# Application arguments (defaults)
# TODO: Check why ARG didn't work properly
# 
# NOTE: As a workaround, a shell script will 
#       take these arguments instead and
#       produce an application.properties file
#	Volume will be hardcoded to /uploads
#
#ARG  DATABASE_PWD=admin
#ARG  DATABASE_USER=root
#ARG  UPLOADS_PATH=${HOME}/upload

# Using latest ubuntu stable version
# since it includes maven in its
# default repo
FROM ubuntu:17.04

LABEL "com.crossover.trial.journals.vendor"="Crossover" \
      version="1.0" \
      description="Crossover Provided Journals Java Application " \
      maintainer="Osvaldo Demo <demoosvaldo@gmail.com>" 

# Latest tomcat
ENV TOMCAT_VERSION 8.5.16

# Install dependencies
RUN apt-get update && apt-get install -y \
    git \ 
    build-essential \ 
    curl \
    wget \
    software-properties-common \
    locales \
    maven \
 && rm -rf /var/lib/apt/lists/*

# Set locales
RUN locale-gen en_NZ.UTF-8
ENV LANG en_NZ.UTF-8
ENV LC_CTYPE en_NZ.UTF-8

# Fix sh
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Install JDK 8
RUN \
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
add-apt-repository -y ppa:webupd8team/java && \
apt-get update && \
apt-get install -y oracle-java8-installer wget unzip tar && \
rm -rf /var/lib/apt/lists/* && \
rm -rf /var/cache/oracle-jdk8-installer

# Define commonly used JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

ADD PDFs /upload

# Start building the application
WORKDIR /app
ADD Code/pom.xml /app/pom.xml
ADD Code/src /app/src
ADD application.properties /app/src/main/resources/application.properties

EXPOSE 8080

VOLUME ["/upload"]
WORKDIR /app

RUN ln -s /upload $HOME/upload 

RUN mvn compile

RUN mvn package

# Launch Tomcat with maven
# TODO: Wrapper script that takes user, password and database name
#	and use docker secrets
CMD ["mvn", "spring-boot:run"]

