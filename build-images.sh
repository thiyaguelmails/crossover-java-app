#!/usr/bin/env bash
#
# 16/07/2017
# Osvaldo Demo
# Version: %%VERSION%%
#
#
# Manually build required docker images on the local host.
# 
# Sript assumes the system is Ubuntu 17.04
# 

DOCKER=`which docker`
SUDO=`which sudo`
PASSWORD="Testing123"
USERNAME="admin"
UPLOADS_PATH="/uploads"
#DOCKER_REGISTRY="hub.docker.com"
# TODO: Optional image push logic

function update_dependencies {
    # Check for sudo first.
    # For Ubuntu should be all good, otherwise fail miserably!
    
    if [[ ! -x ${SUDO} ]]; then
	if [[ $UID -ne 0 ]]; then
		echo "sudo is not installed!. Failing since you need to add yourself to sudoers."
		exit 2
	else
		echo "You're runnig this script as root. Will remove sudo from the command line."
		SUDO=""
	fi
    fi    

    if [[ ! -x ${DOCKER} ]]; then
        
	echo "Docker was not found on the system's PATH"
	echo "Installing..."
	
	# Install required docker dependencies        
	${SUDO} apt-get install \
    		apt-transport-https \
    		ca-certificates \
    		curl \
    		software-properties-common -y

	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	
	# Setup docker repository
	${SUDO} add-apt-repository \
   		"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   		$(lsb_release -cs) \
   		stable"   
	
	# Install docker
	${SUDO} apt-get update
	${SUDO} apt-get install docker-ce -y 
	
	# Add current user to the docker group
	${SUDO} usermod -a -G docker $(sudo who am i|awk '{print $1}')
	
	# Enable docker service 
	${SUDO} systemctl enable docker
	${SUDO} systemctl start docker	
	
	docker info
    fi
}

function usage {
cat << EOF
usage: $0 [-h] [-d] [-u <username>] [-p <password>] [-f <upload files path>] [-r <registry_url>]

This scirpt builds the journals application and database docker containers locally.

        -h  This help
        -d  Debug mode
        -p  database password
        -u  database username
	-f  uploads absolute file path
	-r  docker registry url

Example:

$0 -f /uploads -p Testin123

EOF
exit 1
}


while getopts "hp:du:n:f:r:" OPTION
do
    case ${OPTION} in
            d) set -x
                ;;
            p) PASSWORD="${OPTARG}"
                ;;
	    u) USERNAME="${OPTARG}"
                ;;
	    f) UPLOADS_PATH="${OPTARG}"
		;;
	    r) DOCKER_REGISTRY="${OPTARG}"
		;;	
            h) usage
            ;;
            \?) usage
            ;;

    esac
done

# TODO: Check for ubuntu distro and exit if not found!

update_dependencies

echo "Running container build process..."

$DOCKER info

# Prepare the application.properties config file
echo "Writing new configuration file"
cat << EOF > application.properties 
spring.datasource.url=jdbc:mysql://journals_db:3306/journals?createDatabaseIfNotExist=true
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.hibernate.ddl-auto=create
spring.jpa.database-platform=org.hibernate.dialect.MySQL5Dialect
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
spring.jpa.properties.hibernate.globally_quoted_identifiers=true
multipart.maxFileSize=1MB
multipart.maxRequestSize=1MB
spring.datasource.username=${USERNAME}
spring.datasource.password=${PASSWORD}
upload-dir=${UPLOADS_PATH}
EOF

echo "Writing new tomcat-users.xml file"
cat << EOF > tomcat-users.xml
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
  <role rolename="admin-gui"/>
  <role rolename="admin-script"/>
  <role rolename="manager-gui"/>
  <role rolename="manager-status"/>
  <role rolename="manager-script"/>
  <role rolename="manager-jmx"/>
  <user name="admin" password="${PASSWORD}" roles="admin-gui,admin-script,manager-gui,manager-status,manager-script,manager-jmx"/>
</tomcat-users>
EOF

echo "Building Journals application container..."
time $DOCKER build -t journals .
if [[ $? -ne 0 ]]; then
	echo "Journals Application container build falied!"
	exit 1
fi

cat << EOF > db/env
MYSQL_DATABASE=journals
MYSQL_ROOT_PASSWORD=${PASSWORD}
MYSQL_USER=${USERNAME}
MYSQL_PASSWORD=${PASSWORD}
EOF

echo "Building Journals Database container..."
source db/env
cd db
time $DOCKER build -t journals-db .
cd -
if [[ $? -ne 0 ]]; then
	echo "Journals Database container build falied!"
	exit 1
fi

if [[ -n ${DOCKER_REGISTRY} ]]; then
	echo
	echo "Pushing to docker registry [${DOCKER_REGISTRY}]"
	echo
	echo "You will be prompted for credentials!" 
	echo
	$DOCKER login ${DOCKER_REGISTRY}
	
fi

echo
echo "Current images:"
$DOCKER images

# TODO: catch signals for proper cleanup
rm -f application.properties
rm -f tomcat-users.xml

echo "Storing tar images in output directory..."
mkdir -p output
time $DOCKER save journals -o output/docker-journals.tar.gz
time $DOCKER save journals-db -o output/docker-journals-db.tar.gz

exit 0

